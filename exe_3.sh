#!/bin/bash

# Ce script est un jeu interactif où l'utilisateur doit deviner un nombre entre 1 et 1000.

# Générer un nombre aléatoire entre 1 et 1000
nombre_aleatoire=$(( ( RANDOM % 1000 )  + 1 ))

# Afficher un message de bienvenue
echo "Bienvenue au jeu de deviner le nombre entre 1 et 1000 xD"

# Fonction pour gérer les réponses de l'utilisateur
gerer_reponse() {
    # Vérifier si la réponse est supérieure au nombre aléatoire
    if [ $1 -gt $nombre_aleatoire ]; then
        difference=$(($1 - $nombre_aleatoire))
        # Indiquer que la réponse est trop grande et fournir une indication sur le nombre de nombres plus petits
        if [ $difference -gt 10 ]; then
            echo "C'est plus petit. Il y a plus de 10 nombres plus petits."
        else
            echo "C'est plus petit. Il y a $difference nombres plus petits."
        fi
    # Vérifier si la réponse est inférieure au nombre aléatoire
    elif [ $1 -lt $nombre_aleatoire ]; then
        difference=$(($nombre_aleatoire - $1))
        # Indiquer que la réponse est trop petite et fournir une indication sur le nombre de nombres plus grands
        if [ $difference -gt 10 ]; then
            echo "C'est plus grand. Il y a plus de 10 nombres plus grands."
        else
            echo "C'est plus grand. Il y a $difference nombres plus grands."
        fi
    # Si la réponse est correcte, afficher un message de félicitations et quitter le jeu
    else
        echo "Vous avez raison!"
        exit 0
    fi
}

# Boucle principale du jeu
while true; do
    # Demander à l'utilisateur d'entrer un nombre ou de taper 'panic!' pour quitter
    read -p "Entrez un nombre (ou écrivez 'panic!' pour quitter) : " reponse

    # Vérifier si l'utilisateur veut quitter
    if [ "$reponse" = "panic!" ]; then
        echo "Vous avez abandonné. Le nombre était $nombre_aleatoire."
        exit 0
    fi

    # Vérifier si la réponse est un nombre valide
    if [[ ! "$reponse" =~ ^[0-9]+$ ]]; then
        echo "Veuillez entrer un nombre valide."
        continue
    fi

    # Appeler la fonction pour gérer la réponse de l'utilisateur
    gerer_reponse $reponse
done

